#!/bin/bash
doc_count=$(yq -d'*' r output-${env}.yaml kind | wc -l);
count=0;
while [ $count -le $(($doc_count -1)) ]; do
  kind=$(yq -d"$count" r output-${env}.yaml kind);
  if [ $kind = "ConfigMap" ]; then
    cm_name=$( yq r -d"$count" output-${env}.yaml metadata.name);
    yq w -i -d"$count" output-${env}.yaml metadata.name ${cm_name}-${version};

    scount=0;
    while [ $scount -le $(($doc_count -1)) ]; do
    kind=$(yq -d"$scount" r output-${env}.yaml kind);
    if [ $kind = "Deployment" ] || [ $kind = "StatefulSet" ]; then
      ctcount=0;
      while [ true ]; do
        if [ "$(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount]" )" != "null" ]; then
          if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom" | grep $cm_name | wc -l) -gt 0 ]; then
            envcount=0;
            while [ true ]; do
              if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom[$envcount].configMapRef.name" ) == $cm_name ]; then
                yq w -i -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom[$envcount].configMapRef.name" $cm_name-${version};
                break;
              elif [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom[$envcount].configMapRef.name") == "null" ]; then
                break;
              else
                envcount=$(( $envcount + 1 ));
              fi;
            done;
          else
            if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.volumes" | grep $cm_name | wc -l) -gt 0 ]; then
              volcount=0;
              while [ true ]; do
                if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.volumes[$volcount].configMap.name" ) == $cm_name ]; then
                  yq w -i -d"$scount" output-${env}.yaml "spec.template.spec.volumes[$volcount].configMap.name" $cm_name-${version};
                  break;
                elif [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.volumes[$volcount].configMap.name") == "null" ]; then
                  break;
                else
                  volcount=$(( $volcount + 1 ));
                fi;
              done;
            fi;
          fi;
          ctcount=$(( $ctcount + 1 ));
        else
          break;
        fi;
      done;
    fi;
    scount=$(( $scount + 1 ));
    done;
  elif [ $kind = "Secret" ]; then
    cm_name=$( yq r -d"$count" output-${env}.yaml metadata.name);
    yq w -i -d"$count" output-${env}.yaml metadata.name ${cm_name}-${version};

    scount=0;
    while [ $scount -le $(($doc_count -1)) ]; do
    kind=$(yq -d"$scount" r output-${env}.yaml kind);
    if [ $kind = "Deployment" ] || [ $kind = "StatefulSet" ]; then
      ctcount=0;
      while [ true ]; do
        if [ "$(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount]" )" != "null" ]; then
          if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom" | grep $cm_name | wc -l) -gt 0 ]; then
            envcount=0;
            while [ true ]; do
              if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom[$envcount].secretRef.name" ) == $cm_name ]; then
                yq w -i -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom[$envcount].secretRef.name" $cm_name-${version};
                break;
              elif [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.containers[$ctcount].envFrom[$envcount].secretRef.name") == "null" ]; then
                break;
              else
                envcount=$(( $envcount + 1 ));
              fi;
            done;
          else
            if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.volumes" | grep $cm_name | wc -l) -gt 0 ]; then
              volcount=0;
              while [ true ]; do
                if [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.volumes[$volcount].secret.secretName" ) == $cm_name ]; then
                  yq w -i -d"$scount" output-${env}.yaml "spec.template.spec.volumes[$volcount].secret.secretName" $cm_name-${version};
                  break;
                elif [ $(yq r -d"$scount" output-${env}.yaml "spec.template.spec.volumes[$volcount].secret.secretName") == "null" ]; then
                  break;
                else
                  volcount=$(( $volcount + 1 ));
                fi;
              done;
            fi;
          fi;
          ctcount=$(( $ctcount + 1 ));
        else
          break;
        fi;
      done;
    fi;
    scount=$(( $scount + 1 ));
    done;
  fi;
  count=$(( $count + 1 ));
done;